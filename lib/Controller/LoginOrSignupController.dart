import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:twitter_plus/View/Dashboard.dart';

import 'Firebase/AuthController.dart';


class LoginOrSignupController extends GetxController {
  static LoginOrSignupController get to => Get.put(LoginOrSignupController());

  var isLoading = false.obs;

  /// TextField Controllers to get data from TextFields
  final loginEmail = TextEditingController();
  final loginPassword = TextEditingController();

  final signUpEmail = TextEditingController();
  final signUpPassword = TextEditingController();
  final signUpUserName = TextEditingController();

  //formkeys for textfeild validation
  final GlobalKey<FormState> loginFormKey = GlobalKey<FormState>();
  final GlobalKey<FormState> signUpFormKey = GlobalKey<FormState>();


  //Call this Function to login
  void loginUser() async {
    isLoading.value = true;
    var message = await AuthController.instance.signIn(loginEmail.text, loginPassword.text);
    if(message == '') {
      Get.offAll(const DashBoardView());
    } else {
      Get.snackbar('Error', message);
    }
    isLoading.value = false;
  }

  //Call this Function to SignUp
  void signUpUser() async {
    isLoading.value = true;
    var message = await AuthController.instance.signUp(signUpEmail.text, signUpPassword.text, signUpUserName.text);
    if(message == '') {
      Get.offAll(const DashBoardView());
    } else {
      Get.snackbar('Error', message);
    }
    isLoading.value = false;
  }



}
