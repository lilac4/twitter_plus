import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:twitter_plus/Controller/Firebase/AuthController.dart';

import '../Model/TextItemModel.dart';


class DashBoardController extends GetxController {

  static DashBoardController get to => Get.put(DashBoardController());

  final tweetField = TextEditingController();
  final updatedTweetField = TextEditingController();
  late List<TweetItem> tweetItems;
  RxBool isLoading = false.obs;

  @override
  void onInit() async {
    // TODO: implement onInit
    super.onInit();
    tweetItems = AuthController.instance.tweetItems;
  }

}