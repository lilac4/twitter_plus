import 'package:get/get.dart';
import 'package:twitter_plus/View/Dashboard.dart';
import 'package:twitter_plus/View/LoginOrSignUp.dart';

import 'Firebase/AuthController.dart';


class SplashController extends GetxController {
  static SplashController get to => Get.put(SplashController());
  
  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    Future.delayed(const Duration(seconds: 3),() {
      final AuthController authController = Get.find();
      authController.isLoggedIn?
      Get.offAll(const DashBoardView()):
      Get.offAll(const LoginOrSignUpView());
    });
  }
  
}
