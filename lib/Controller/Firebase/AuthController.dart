import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:twitter_plus/Controller/DashBoardController.dart';
import 'package:twitter_plus/Model/UserModel.dart';

import '../../Model/TextItemModel.dart';

class AuthController extends GetxController {
  static AuthController get instance => Get.find();

  //Variables
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final Rxn<User> _firebaseUser = Rxn<User>();
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;

  Rx<List<TweetItem>> tweetList = Rx<List<TweetItem>>([]);

  List<TweetItem> get tweetItems => tweetList.value;

  User? get user => _firebaseUser.value;

  @override
  void onInit() {
    super.onInit();
    _firebaseUser.bindStream(_auth.authStateChanges());
    fetchTextListFromFirestore();
  }

  bool get isLoggedIn => _firebaseUser.value != null;

  Future<String> signUp(String email, String password, String userName) async {
    String errorMsg = '';
    try {
      await _auth.createUserWithEmailAndPassword(
          email: email, password: password);
      User? firebaseUser = _auth.currentUser;
      print('user=>${firebaseUser?.displayName.toString()}');
      if (firebaseUser != null) {
        print('firebaseUser=>${firebaseUser.toString()}');
        await firebaseUser.updateDisplayName(userName);
        await _firestore.collection('users').doc(firebaseUser.uid).set({
          'username': userName,
        });
        Get.snackbar('Success>', 'data added');
      }
      return errorMsg;
    } catch (e) {
      errorMsg = e.toString();
      return errorMsg;
    }
  }

  String? getUserName() {
    return _auth.currentUser?.displayName;
  }

  Future<String> signIn(String email, String password) async {
    String errorMsg = '';
    try {
      await _auth.signInWithEmailAndPassword(email: email, password: password);
      return errorMsg;
    } catch (e) {
      errorMsg = e.toString();
      return errorMsg;
    }
  }

  Future<String> signOut() async {
    String errorMsg = '';
    try {
      await _auth.signOut();
      return errorMsg;
    } catch (e) {
      errorMsg = e.toString();
      return errorMsg;
    }
  }

  Future<void> saveTweetListToFirestore({required String tweet}) async {
    try {
      await _firestore.collection('users').doc(_auth.currentUser?.uid).collection('tweets').doc().set({
          'tweet': tweet,
          'datetime': DateTime.now(),
        }).whenComplete(
              () async {
            await fetchTextListFromFirestore();
            Get.back();
          });
      Get.snackbar('Success', 'Tweet list saved');
    } catch (e) {
      Get.snackbar('Error', e.toString());
    }
  }

  Future<void> fetchTextListFromFirestore() async {
    try {
      final snapshot = await _firestore.collection('users').doc(_auth.currentUser?.uid).collection('tweets').get();
      List<TweetItem> tempList = snapshot.docs.map((doc) {
        return TweetItem(
          id: doc.id,
          tweet: doc['tweet'],
          datetime: doc['datetime'].toDate(),
        );
      }).toList();
      tweetList.value = tempList;
      DashBoardController.to.update();
    } catch (e) {
      Get.snackbar('Error', e.toString());
    }
  }

  Future<void> updateTweetInFirestore(int documentId, String newText) async {
    try {
      CollectionReference collection = FirebaseFirestore.instance.collection('users').doc(_auth.currentUser?.uid).collection('tweets');

      QuerySnapshot querySnapshot = await collection.get();

      List<DocumentSnapshot> documents = querySnapshot.docs;

      await _firestore.collection('users').doc(_auth.currentUser?.uid).collection('tweets').doc(documents[documentId].id).update({'tweet': newText, 'datetime': DateTime.now()}).whenComplete(
              () async {
                await fetchTextListFromFirestore();
                Get.back();
              });
      Get.snackbar('Success', 'Text updated in Firestore');
    } catch (e) {
      Get.snackbar('Error', e.toString());
    }
  }

  Future<void> deleteTweetFromFirestore(int documentId) async {
    try {
      CollectionReference collection = FirebaseFirestore.instance.collection('users').doc(_auth.currentUser?.uid).collection('tweets');

      QuerySnapshot querySnapshot = await collection.get();

      List<DocumentSnapshot> documents = querySnapshot.docs;

      await _firestore.collection('users').doc(_auth.currentUser?.uid).collection('tweets').doc(documents[documentId].id).delete().whenComplete(
              () async {
            await fetchTextListFromFirestore();
          });
      Get.snackbar('Success', 'Tweet deleted from Firestore');
    } catch (e) {
      Get.snackbar('Error', e.toString());
    }
  }
}
