class TweetItem {
  final String id;
  final String tweet;
  final DateTime datetime;

  TweetItem({required this.id, required this.tweet, required this.datetime});
}