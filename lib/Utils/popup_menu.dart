import 'package:flutter/material.dart';

import 'custom_text.dart';

class PopUpMenuTile extends StatelessWidget {
  const PopUpMenuTile(
      {required this.title});
  final String title;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        CustomText(title),
      ],
    );
  }
}