
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class AppTextField extends StatelessWidget {
  final TextInputType keyboardType;
  final TextEditingController? controller;
  final bool validator;
  final String hint;
  final int? maxLength;

  AppTextField(
      {this.keyboardType = TextInputType.text,
      required this.controller,
      this.validator = false,
      required this.hint,
        this.maxLength});
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      keyboardType: keyboardType,
      enableInteractiveSelection: true,
      controller: controller,
      autovalidateMode: AutovalidateMode.onUserInteraction,
      validator: (value) {
        if (validator) {
          return validateEmpty(controller!.text);
        }
        return null;
      },
      decoration: InputDecoration(
          hintText: hint,
          floatingLabelBehavior: FloatingLabelBehavior.always,
          focusedBorder: border,
          border: border,
          enabledBorder: border,
          errorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10.r),
            borderSide: const BorderSide(
              width: 1,
              color: Colors.redAccent,
            ),
          ),
          errorStyle: const TextStyle(height: 0),
          hintStyle: const TextStyle(
              color: Color.fromRGBO(37, 37, 37, 0.4),
    )));
  }

  OutlineInputBorder get border => OutlineInputBorder(
        borderRadius: BorderRadius.circular(10.r),
        borderSide: const BorderSide(
          width: 1,
          color: Color(0xff4C9EEB),
        ),
      );
}

String? validateEmpty(String value) {
  if (value.isEmpty) {
    return "";
  }
  return null;
}
