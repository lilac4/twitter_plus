import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:twitter_plus/Utils/app_text_field.dart';
import 'package:twitter_plus/Utils/custom_text.dart';
import 'package:twitter_plus/View/LoginOrSignUp.dart';

import '../Controller/DashBoardController.dart';
import '../Controller/Firebase/AuthController.dart';
import '../Model/TextItemModel.dart';
import '../Utils/popup_menu.dart';

class DashBoardView extends StatelessWidget {
  const DashBoardView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: GetBuilder<DashBoardController>(
          init: DashBoardController(),
          builder: (c) {
            return Scaffold(
              appBar: AppBar(
                backgroundColor: Colors.white,
                shadowColor: Colors.white,
                toolbarHeight: 79.h,
                flexibleSpace: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 14.w, vertical: 8.h),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            IconButton(
                                onPressed: () {},
                                icon: Icon(
                                  Icons.account_circle_outlined,
                                  size: 28.r,
                                )),
                            CustomText(
                                AuthController.instance.getUserName()?.toUpperCase() ??
                                    'Guest', color: Colors.blue, fontWeight: FontWeight.w700,fontSize: 14.sp,),
                          ]),
                      SvgPicture.asset(
                        'assets/images/twitter_icon.svg',
                        width: 25.w,
                        height: 25.h,
                      ),
                      PopupMenuButton<int>(
                          offset: const Offset(0, -380),
                          itemBuilder: (context) => [
                            PopupMenuItem<int>(
                                value: 0,
                                child: GestureDetector(
                                  onTap: () async {
                                    await AuthController.instance
                                        .signOut();
                                    Get.offAll(const LoginOrSignUpView());
                                  },
                                  child: const PopUpMenuTile(
                                    title: 'Logout',
                                  ),
                                )),
                          ],
                          child: Icon(Icons.settings, size: 28.r))
                    ],
                  ),
                ),
              ),
              body: Padding(
                padding: EdgeInsets.symmetric(horizontal: 14.w),
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      SizedBox(height: 20.h),
                      Column(
                        children: [
                          Column(
                            children: [
                              Container(
                                width: Get.width * 0.9,
                                padding: EdgeInsets.symmetric(
                                    horizontal: 10.w, vertical: 10.h),
                                child: TextFormField(
                                    keyboardType: TextInputType.text,
                                    enableInteractiveSelection: true,
                                    controller: c.tweetField,
                                    showCursor: true,
                                    autovalidateMode:
                                    AutovalidateMode.onUserInteraction,
                                    maxLength: 280,
                                    decoration: InputDecoration(
                                        hintText: "You can tweet here...",
                                        floatingLabelBehavior:
                                        FloatingLabelBehavior.always,
                                        focusedBorder: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(10.r),
                                          borderSide: const BorderSide(
                                            width: 1,
                                            color: Color(0xff4C9EEB),
                                          ),
                                        ),
                                        border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(10.r),
                                          borderSide: const BorderSide(
                                            width: 1,
                                            color: Color(0xff4C9EEB),
                                          ),
                                        ),
                                        enabledBorder: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(10.r),
                                          borderSide: const BorderSide(
                                            width: 1,
                                            color: Color(0xff4C9EEB),
                                          ),
                                        ),
                                        hintStyle: const TextStyle(
                                          color: Color.fromRGBO(37, 37, 37, 0.4),
                                        ))),
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  GestureDetector(
                                    onTap: () async {
                                      FocusScope.of(context).unfocus();
                                      await AuthController.instance.saveTweetListToFirestore(tweet: c.tweetField.text).whenComplete(() {
                                        c.onInit();
                                        c.tweetField.clearComposing();
                                        c.tweetField.clear();
                                        c.update();
                                      });
                                    },
                                    child: Container(
                                      height: 30.h,
                                      width: 60.w,
                                      margin: EdgeInsets.only(right: 14.w),
                                      decoration: BoxDecoration(
                                          borderRadius: BorderRadius.all(Radius.circular(5.r)),
                                          color: const Color(0xff4C9EEB)
                                      ),
                                      child: GetX<DashBoardController>(
                                          builder: (c) {
                                            return Center(
                                              child: c.isLoading.value?
                                              const CircularProgressIndicator(color: Colors.white):
                                              Icon(Icons.send_rounded,color: Colors.white54,size: 18.sp),
                                            );
                                          }
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                          SizedBox(height: 40.h),
                          c.tweetItems.isNotEmpty && c.tweetItems != null?
                          ListView.builder(
                            itemCount: c.tweetItems.length,
                            shrinkWrap: true,
                            physics: const NeverScrollableScrollPhysics(),
                            itemBuilder: (context, index) {
                              TweetItem item = c.tweetItems[index];
                              return Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Row(
                                    children: [
                                      Container(
                                        child: Center(
                                          child: Icon(Icons.account_circle_outlined,size: 30.sp,color: Colors.blue,),
                                        ),
                                      ),
                                      SizedBox(width: 10.w,),
                                      Container(
                                        child: Column(
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            CustomText(item.tweet,fontWeight: FontWeight.w700),
                                            Padding(
                                              padding: EdgeInsets.symmetric(vertical: 8.h),
                                              child: CustomText(DateFormat('yyyy-MM-dd HH:mm:ss').format(item.datetime),
                                                  fontWeight:
                                                  FontWeight.w400,
                                                  color: Colors.grey),
                                            ),
                                            Row(
                                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                              children: [
                                                IconButton(onPressed: () async {
                                                  await AuthController.instance.deleteTweetFromFirestore(index).whenComplete(() {
                                                    c.onInit();
                                                    c.update();
                                                  });
                                                },
                                                    icon: Icon(Icons.delete,color: Colors.blue,size: 25.sp)),
                                                SizedBox(width: 50.w),
                                                IconButton(onPressed: () {
                                                  c.updatedTweetField.text = item.tweet;
                                                  Get.bottomSheet(
                                                      Container(
                                                        decoration: BoxDecoration(
                                                            color: Colors.white,
                                                            borderRadius: BorderRadius.only(
                                                                topLeft: Radius.circular(20.r),
                                                                topRight: Radius.circular(20.r)
                                                            )
                                                        ),
                                                        child:  Column(
                                                            children: [
                                                              Container(
                                                                width: Get.width * 0.9,
                                                                padding: EdgeInsets.symmetric(
                                                                    horizontal: 10.w, vertical: 10.h),
                                                                child: TextFormField(
                                                                    keyboardType: TextInputType.text,
                                                                    enableInteractiveSelection: true,
                                                                    controller: c.updatedTweetField,
                                                                    showCursor: true,
                                                                    autovalidateMode:
                                                                    AutovalidateMode.onUserInteraction,
                                                                    maxLength: 280,
                                                                    decoration: InputDecoration(
                                                                        hintText: "You can tweet here...",
                                                                        floatingLabelBehavior:
                                                                        FloatingLabelBehavior.always,
                                                                        focusedBorder: OutlineInputBorder(
                                                                          borderRadius: BorderRadius.circular(10.r),
                                                                          borderSide: const BorderSide(
                                                                            width: 1,
                                                                            color: Color(0xff4C9EEB),
                                                                          ),
                                                                        ),
                                                                        border: OutlineInputBorder(
                                                                          borderRadius: BorderRadius.circular(10.r),
                                                                          borderSide: const BorderSide(
                                                                            width: 1,
                                                                            color: Color(0xff4C9EEB),
                                                                          ),
                                                                        ),
                                                                        enabledBorder: OutlineInputBorder(
                                                                          borderRadius: BorderRadius.circular(10.r),
                                                                          borderSide: const BorderSide(
                                                                            width: 1,
                                                                            color: Color(0xff4C9EEB),
                                                                          ),
                                                                        ),
                                                                        hintStyle: const TextStyle(
                                                                          color: Color.fromRGBO(37, 37, 37, 0.4),
                                                                        ))),
                                                              ),
                                                              Row(
                                                                mainAxisAlignment: MainAxisAlignment.end,
                                                                children: [
                                                                  GestureDetector(
                                                                    onTap: () async {
                                                                      FocusScope.of(context).unfocus();
                                                                      await AuthController.instance.updateTweetInFirestore(index, c.updatedTweetField.text).whenComplete(() {
                                                                        c.onInit();
                                                                        c.update();
                                                                      });
                                                                    },
                                                                    child: Container(
                                                                      height: 30.h,
                                                                      width: 60.w,
                                                                      margin: EdgeInsets.only(right: 14.w),
                                                                      decoration: BoxDecoration(
                                                                          borderRadius: BorderRadius.all(Radius.circular(5.r)),
                                                                          color: const Color(0xff4C9EEB)
                                                                      ),
                                                                      child: GetX<DashBoardController>(
                                                                          builder: (c) {
                                                                            return Center(
                                                                              child: c.isLoading.value?
                                                                              const CircularProgressIndicator(color: Colors.white):
                                                                              Icon(Icons.send_rounded,color: Colors.white54,size: 18.sp),
                                                                            );
                                                                          }
                                                                      ),
                                                                    ),
                                                                  ),
                                                                ],
                                                              ),
                                                            ]),
                                                      )
                                                  );
                                                },
                                                    icon: Icon(Icons.edit,color: Colors.blue,size: 25.sp)),
                                              ],
                                            )
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                  Container(
                                    height: 1.h,
                                    width: Get.width,
                                    color: Colors.grey,
                                  ),
                                  SizedBox(height: 15.h,)
                                ],
                              );
                            },):
                          Expanded(
                              child: CustomText(
                                'No tweets available',
                                color: Colors.grey,
                              ))
                        ],
                      )
                    ],
                  ),
                ),
              ),
            );
          }),
    );
  }
}
