import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:twitter_plus/Controller/LoginOrSignupController.dart';
import 'package:twitter_plus/Utils/app_text_field.dart';
import 'package:twitter_plus/Utils/custom_text.dart';

class LoginOrSignUpView extends StatelessWidget {
  const LoginOrSignUpView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: GetBuilder<LoginOrSignupController>(
          init: LoginOrSignupController(),
          builder: (c) {
            return Scaffold(
              body: Container(
                width: Get.width,
                padding: EdgeInsets.symmetric(horizontal: 14.w),
                decoration: const BoxDecoration(
                    color: Colors.white12,
                ),
                child: Form(
                  key: c.loginFormKey,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      const Spacer(),
                      SvgPicture.asset(
                        'assets/images/twitter_icon.svg',
                        width: 100.w,
                        height: 100.h,
                      ),
                      const Spacer(),
                      SizedBox(
                          height: 45.h,
                          child: AppTextField(controller: c.loginEmail,
                            hint: 'Enter email',
                            validator: true,
                          )),
                      SizedBox(height: 20.h),
                      SizedBox(
                          height: 45.h,
                          child: AppTextField(controller: c.loginPassword,
                            hint: 'Enter password',
                            validator: true,
                          )),
                      SizedBox(height: 30.h),
                      Row(
                        children: [
                          CustomText('New to Twitter Plus ?...      ',color: Colors.grey,fontSize: 15.sp,fontWeight: FontWeight.w500,),
                          InkWell(
                            onTap: () {
                              Get.bottomSheet(
                                  isDismissible: true,
                                  enableDrag: false,
                                  SignUpBottom());
                            },
                              child: CustomText('Create an account',color: Colors.blue,fontSize: 18.sp,underline: true,fontWeight: FontWeight.w500,)),
                        ],
                      ),
                      const Spacer(),
                      GestureDetector(
                        onTap: () {
                          bool isValid = c.loginFormKey.currentState!.validate();
                          if (isValid) {
                             c.loginUser();
                          }
                        },
                        child: Container(
                          height: 45.h,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(8.r)),
                              color: Color(0xff4C9EEB)
                          ),
                          child: GetX<LoginOrSignupController>(
                            builder: (c) {
                              return Center(
                                child: c.isLoading.value?
                                const CircularProgressIndicator(color: Colors.white):
                                CustomText('Login',color: Colors.white,fontSize: 18.sp,fontWeight: FontWeight.w500,),
                              );
                            }
                          ),
                        ),
                      ),
                      SizedBox(height: 60.h)
                    ],
                  ),
                ),
              ),
            );
          }
        ));
  }
}

class SignUpBottom extends StatelessWidget {
  const SignUpBottom({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<LoginOrSignupController>(
      builder: (c) {
        return Container(
          padding: EdgeInsets.only(
            left: 15.w,
            right: 15.w,
            top: 40.h
          ),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20.r),
              topRight: Radius.circular(20.r)
            )
          ),
          child: Form(
            key: c.signUpFormKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(
                    height: 45.h,
                    child: AppTextField(controller: c.signUpUserName,
                        hint: 'Enter a user name',
                      validator: true,
                    )),
                SizedBox(height: 20.h),
                SizedBox(
                    height: 45.h,
                    child: AppTextField(controller: c.signUpEmail,
                        hint: 'Enter your email',
                      validator: true,
                    )),
                SizedBox(height: 20.h),
                SizedBox(
                    height: 45.h,
                    child: AppTextField(controller: c.signUpPassword,
                        hint: 'Enter a password',
                      validator: true,
                    )),
                const Spacer(),
                GestureDetector(
                  onTap: () {
                    bool isValid = c.signUpFormKey.currentState!.validate();
                    if (isValid) {
                      c.signUpUser();
                    }
                  },
                  child: Container(
                    height: 45.h,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(8.r)),
                        color: Color(0xff4C9EEB)
                    ),
                    child: GetX<LoginOrSignupController>(
                        builder: (c) {
                          return Center(
                            child: c.isLoading.value?
                            const CircularProgressIndicator(color: Colors.white):
                            CustomText('Signup',color: Colors.white,fontSize: 18.sp,fontWeight: FontWeight.w500,),
                          );
                        }
                    ),
                  ),
                ),
                const Spacer(),
              ],
            ),
          ),
        );
      }
    );
  }
}
