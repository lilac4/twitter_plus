import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/src/simple/get_state.dart';

import '../Controller/SplashController.dart';
import '../Utils/custom_text.dart';

class SplashView extends StatelessWidget {
  const SplashView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: GetBuilder<SplashController>(
            init: SplashController(),
            builder: (c) {
              return Scaffold(
                body: Container(
                  width: Get.width,
                  decoration: const BoxDecoration(
                      gradient: LinearGradient(
                          begin: Alignment.bottomLeft,
                          end: Alignment.topRight,
                          colors: [Color.fromRGBO(0, 143, 231, 1),
                            Color.fromRGBO(0, 85, 137, 1)])
                  ),
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children :[
                        SvgPicture.asset(
                          'assets/images/twitter_icon.svg',
                          width: 100.w,
                          height: 100.h,
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 10.h),
                          child: CustomText('Twitter Plus',
                              fontSize: 35.sp,
                              color: Colors.white,
                              fontWeight: FontWeight.w600),
                        )
                      ]
                  ),
                ),
              );
            }
        ));
  }
}
